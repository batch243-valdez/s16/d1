// console.log("Hello World");

// [Section] - Arithmetic Operators -

let x = 1397;
console.log("x: " +x);
let y = 7831;
console.log("y: " +y);
// Addition operator
let sum = x + y;
console.log("Result of addition operator: " +sum);

// Difference Operator
let difference = x - y;
console.log("Result of Subtraction operator " +difference);

// Multipication Operator
let product = x * y;
console.log("Result of Multiplication operator " +product);

// Division Operator
let quotient = x / y;
console.log("Result of Division operator " +quotient);

// Modulo
let remainder = y % x;
console.log("Result of Modulo: " +remainder);

// [Section] --- Assignment Operators ---
	// Basic assignment operator (=)
	// The assignment operator adds the value of the right operand to a variable and assigns the result to the value.

	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition Assignment Operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	// assignmentNumber = assignmentNumber + 2;
	// console.log(assignmentNumber);

	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " +assignmentNumber);

	// Subtraction assignment operator (-=)
	assignmentNumber -= 2;
	console.log("Result of addition assignment operator: " +assignmentNumber);

	// Multiplication assignment operator (*=)
	assignmentNumber *= 4;
	console.log("Result of Multiplication assignment operator: " +assignmentNumber);

	// Multiplication assignment operator (/=)
	assignmentNumber /= 8;
	console.log("Result of Division assignment operator: " +assignmentNumber);
// [Section] --- Multiple Operators and Parentheses ---
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of MDAS rule: " +mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of PEMDAS rule: " +pemdas);

// [Section] Incrementation and Decrement
	// Operators that add or subract values by 1 and reassigns the value of the variable where the increment/decrement was applied to 

	let z = 1;

	// Pre-increment
	let increment = ++z;
	console.log("Result of z in pre-increment: " +z);
	console.log("Result of increment in pre-increment: " +increment);

	// Post-increment
	increment = z++;
	console.log("Result of z in pre-increment: " +z);
	console.log("Result of increment in pre-increment: " +increment);
	increment = z++;
	console.log("Result of increment in pre-increment: " +increment);

	let p = 0;
	// pre-decrement
	
	let decrement = --p;
	console.log("Result of p in pre-decrement: " +p);
	console.log("Result of decrement in pre-decrement: " +decrement);

	// post-decrement
	
	decrement = p--;
	console.log("Result of p in post-decrement: " +p);
	console.log("Result of decrement in post-decrement: " +decrement);
	decrement = p--;
	console.log("Result of decrement in post-decrement: " +decrement);
// [Section --- Type Coercion ---]
	/*
		-type coercion is the automatic implicit conversion of values from one data type to another
		-this happens when operations are performed on different data types that would normally not be possible and yield irregular results.
		-Values are automatically converted from one data type to another in order to resolve operations.
	*/

	let numA = '10';
	let numB = 12;
		/*
			-adding or concatinating a string and a number will result
			-This can be proven in the console by looking at the color of the text displayed. 
		*/

	let coercion = numA + numB;
	console.log(coercion);

	// non-coercion
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	
	let numE = false + 1;
	console.log(numE);
	/*
		type Coercion: Boolean value and number
		The result will be a number
		The boolean value will be converted. true = 1, false = 0.
	*/

// [Section] --- Comparison Operators ---
	let juan = "juan";

	// Equality operator (==)
	/*
		-checks whether the operands are equal/have the same value
	*/
	let isEqual = 1 == 1;
	console.log(typeof isEqual);

	console.log(1 == 2);
	console.log(1 == "1");

	// Strict equality operator (===)
	console.log(1 === "1");
	console.log("juan" == "juan");
	console.log("juan" == juan);

	// Inequality operator (!=)
	/*
		-checks whether the operands are not equal/have different content
		-attemtps to convert and compare operand of different data types
	*/

	console.log(1 != 1);

// [Section] Relational Operators
	// some comparison operators check whether one value is greater or less than to the other value.

	let a = 50;
	let b = 65;

	// GT or Greater than operator
	// let isGreaterThan = 

